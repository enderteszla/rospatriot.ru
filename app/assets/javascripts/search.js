jQuery(function($){
    $('#header-search').find('[type="text"]').on('keyup', function(e){
        var form = $(e.target).parents('form'),
            container = $('#search-results');
        $.post(form.attr('action'), form.serializeArray(), function(data){
            container.html(data);
            if(data.length > 0)
                container.show();
            else
                container.hide();
        }, 'html');
    });
});