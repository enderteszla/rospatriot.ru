class AddressesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :set_address, :except => [:index, :create]

  def index
    @addresses = current_user.addresses.page params[:page]
  end

  def create
    @address = current_user.addresses.new update_params
    unless @address.save
      flash[:error] = @address.errors.full_messages
    end
    redirect_to addresses_url
  end

  def update
    unless @address.update update_params
      flash[:error] = @address.errors.full_messages
    end
    redirect_to addresses_url
  end

  def destroy
    @address.update :user => nil
    redirect_to addresses_url
  end

  private
  def set_address
    @address = (Address.find_by_id(params[:id]) or not_found)
  end

  def update_params
    params.require(:address)
        .permit([:first_name, :last_name, :phone, :city, :index, :street, :house, :housing, :build, :flat, :default])
  end
end
