class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :set_navigation
  before_filter :set_cart

  protected
  def not_found
    raise ActionController::RoutingError.new('Not Found')
  end

  def product_add_to_cart(product, data)
    order = Order.find_by_id session[:order_id]
    product_record = order.order_records.create :product => product, :price => product.real_price
    if product.lettering
      product_record.update data.permit(:quantity, :color, :size, :lettering, :lettering_color, :lettering_text, :lettering_format_id)
    else
      product_record.update data.permit(:quantity, :color, :size)
    end
  end

  private
  def set_navigation
    @nav = {
        :categories => CategoryTag.all,
        :regions => RegionTag.all.order(:order),
        :subjects => SubjectTag.all,
        :types => TypeTag.all,
    }
  end

  def set_cart
    @cart = Order.find_by_id session[:order_id]
    if user_signed_in?
      if @cart && @cart.created?
        if @cart.user_id != current_user.id
          if @cart.user.nil?
            @cart.update :user => current_user
            @cart = Order.merge @cart, current_user.orders.find_by(:status => :created)
          else
            @cart = current_user.orders.find_or_create_by :status => :created
          end
        end
      else
        @cart = current_user.orders.find_or_create_by :status => :created
      end
    else
      @cart = Order.create :status => :created unless @cart && @cart.created? && @cart.user.nil?
    end
    session[:order_id] = @cart.id
  end
end
