class CategoryTagsController < ApplicationController
  before_filter :set_navigation

  def index
  end

  def show
    @category = (CategoryTag.find_by_id(params[:id]) or not_found)
  end
end
