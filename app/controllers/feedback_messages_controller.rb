class FeedbackMessagesController < ApplicationController
  def new
  end

  def create
    FeedbackMailer.notify(create_params).deliver_later
  end

  private
  def create_params
    params.require(:feedback_message)
        .permit([:name, :phone, :email, :message])
  end
end
