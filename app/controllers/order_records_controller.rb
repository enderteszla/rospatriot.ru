class OrderRecordsController < ApplicationController
  before_filter :set_navigation

  def update
    OrderRecord.find_by_id(params[:id]).update params.require(:order_record).permit(:quantity)
    redirect_to orders_url
  end

  def destroy
    OrderRecord.find_by_id(params[:id]).try :destroy!
    redirect_to orders_url
  end
end
