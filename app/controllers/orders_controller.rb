class OrdersController < ApplicationController
  before_filter :set_navigation, :except => [:check, :pay, :success, :fail]
  before_filter :set_order, :except => [:index, :check, :pay, :success, :fail]
  skip_before_filter :verify_authenticity_token, :only => [:check, :pay, :success, :fail]
  skip_before_filter :set_cart, :only => [:check, :pay, :success, :fail]
  skip_before_filter :set_navigation, :only => [:check, :pay, :success, :fail]

  def index
    @order = @cart
    set_route
    @route[:current] = :index
  end

  def edit
    redirect_to @order and return unless @order.created?
    redirect_to orders_url and return if @order.empty?
    set_route
    @route[:current] = :edit
    @user = User.new unless user_signed_in?
    @address = Address.new unless (current_user.try(:addresses) or []).count > 0
  end

  def update
    _params = update_params
    unless @order.user
      @user = User.new user_params
      if @user.save
        @user.update :confirmed_at => DateTime.now
        @order.update :user => @user
        sign_in @user
        @user.update :confirmed_at => nil
      else
        @user.errors.each { |k,v| @order.errors.add(k, v) } if @user.errors.any?
      end
    end
    unless @order.address
      if _params[:address_id]
        @order.update :address_id => _params[:address_id]
      else
        @address = Address.new address_params.merge({:user_id => @order.user_id})
        if @address.save
          @order.update :address_id => @address.id
        else
          @address.errors.each { |k,v| @order.errors.add(k, v) } if @address.errors.any?
        end
      end
    end
    if @order.errors.count == 0 && @order.user && @order.address && @order.update(update_params)
      OrderMailer.notify(@order.order_records.map{|r| {:name => r.product.name, :quantity => r.quantity, :price => r.price} }).deliver_later
      OrderMailer.notify_user(@order.order_records.map{|r| {:name => r.product.name, :quantity => r.quantity, :price => r.price} }, @order.user.email).deliver_later
      if @order.cash?
        @order.update :yandex_delivery_id => (YandexDelivery.create_order @order) unless @order.pickup?
        redirect_to @order and return
      end
      redirect_to YandexMoney::RequestGenerator.new.generate(@order.id)
    else
      set_route
      @route[:current] = :edit
      flash.now[:error] = @order.errors.full_messages
      render :edit
    end
  end

  def show
    redirect_to edit_order_url(@order) and return if @order.created?
    set_route
    @route[:current] = :show
  end

  def check
    render :text => YandexMoney::ResponseGenerator.new.check(params)
  end

  def pay
    render :text => YandexMoney::ResponseGenerator.new.pay(params)
  end

  def success
    order = (Order.find_by_id(params[:orderNumber]) or not_found)
    redirect_to order
  end

  def fail
    order = (Order.find_by_id(params[:orderNumber]) or not_found)
    order.created!
    redirect_to order
  end

  private
  def set_order
    @order = (Order.find_by_id(params[:id]) or not_found)
  end

  def set_route
    @route = {
        :pages => {
            :index => {
                :url => orders_url,
                :title => "Корзина",
                :disabled => !@order.created?
            },
            :edit => {
                :url => edit_order_url(@order),
                :title => "Оформление",
                :disabled => !@order.created? || @order.empty?
            },
            :show => {
                :url => order_url(@order),
                :title => "Подтверждение",
                :disabled => @order.created?
            }
        }
    }
  end

  def update_params
    params.require(:order)
        .permit([:address_id, :delivery_type, :payment_type])
        .merge({:status => :confirmed})
  end

  def user_params
    params.require(:user)
        .permit([:email, :password])
  end

  def address_params
    params.require(:address)
        .permit([:first_name, :last_name, :phone, :city, :index, :street, :house, :housing, :build, :flat])
  end
end
