class ProductsController < ApplicationController
  def index
    @products = Product.active.page params[:page]
  end

  def show
    if params[:type_id]
      type = (TypeTag.find_by_id params[:type_id] or not found)
    end
    case params[:by]
      when "subject"
        subject = (SubjectTag.find_by_id params[:subject_id] or not_found)
        @route = {
            :group => {:name => "Общие товары", :path => subjects_url},
            :item => {:name => subject.name, :path => subject_url(subject)}
        }
        @route[:type] = {:name => type.name, :path => subject_type_url(:subject_id => subject.id, :id => type.id)} if type
      when "category"
        category = (CategoryTag.find_by_id params[:category_id] or not_found)
        @route = {
            :group => {:name => "Типы товаров", :path => categories_url},
            :item => {:name => category.name, :path => category_url(category)}
        }
        @route[:type] = {:name => type.name, :path => category_type_url(:category_id => category.id, :id => type.id)} if type
      when "region"
        region = (RegionTag.find_by_id params[:region_id] or not_found)
        @route = {
            :group => {:name => "Региональные товары", :path => regions_url},
            :item => {:name => region.name, :path => region_url(region)}
        }
        @route[:type] = {:name => type.name, :path => region_type_url(:region_id => region.id, :id => type.id)} if type
      when "shop"
        @route = {}
        @route[:type] = {:name => type.name, :path => root_url(:anchor => "scroll_link_type_#{type.id}")} if type
      else
        not_found
    end
    @product = (Product.active.where(:id => params[:id]).first or not_found)
    @lettering_formats = LetteringFormat.all
  end

  def search
    @query = params[:query]
    @products = Product.active.search(params[:query]).page params[:page]
  end

  def search_live
    render :partial => "search", :locals => {:products => Product.active.search(params[:query])}
  end

  def create
    @product = (Product.active.where(:id => params[:id]).first or not_found)
    product_add_to_cart @product, params[:data]
    redirect_to params[:return_back] == "true" ? @product : orders_url
  end
end
