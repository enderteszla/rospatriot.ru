class RegionTagsController < ApplicationController
  def index
  end

  def show
    @region = (RegionTag.find_by_id(params[:id]) or not_found)
  end

  def video
    @region = (RegionTag.find_by_id(params[:id]) or not_found)
    render :partial => "video"
  end
end
