class SiteController < ApplicationController
  before_filter :authenticate_user!, :only => :account

  def index
    @articles = Article.order(:created_at => :asc).reverse_order.limit(2).reverse
    @banners = Banner.all
  end

  def mass_media
    @articles = Article.all.page params[:page]
  end

  def may_9th
  end

  def news
    @news = News.all.page params[:page]
  end

  def about
  end

  def competition
  end

  def exclusive
  end

  def faq
  end

  def exclusive_gallery
  end

  def account
  end

  def oferta_popup
    render :partial => "oferta_popup"
  end

  def exclusive_popup
    render :partial => "exclusive_popup"
  end

  def video_popup
    render :partial => "video_popup"
  end

  def product_popup
    render :partial => "product_popup"
  end
end
