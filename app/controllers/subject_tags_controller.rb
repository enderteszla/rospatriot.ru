class SubjectTagsController < ApplicationController
  def index
  end

  def show
    @subject = (SubjectTag.find_by_id(params[:id]) or not_found)
  end
end
