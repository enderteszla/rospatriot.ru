class TypeTagsController < ApplicationController
  def show
    case params[:by]
      when "subject"
        subject = (SubjectTag.find_by_id params[:subject_id] or not_found)
        product_ids = subject.products_active.collect(&:id)
        @route = {
            :group => {:name => "Общие товары", :path => subjects_url, :class => "subject"},
            :item => {:name => subject.name, :path => subject_url(subject), :id => subject.id}
        }
      when "category"
        category = (CategoryTag.find_by_id params[:category_id] or not_found)
        product_ids = category.products_active.collect(&:id)
        @route = {
            :group => {:name => "Типы товаров", :path => categories_url, :class => "category"},
            :item => {:name => category.name, :path => category_url(category), :id => category.id}
        }
      when "region"
        region = (RegionTag.find_by_id params[:region_id] or not_found)
        product_ids = region.products_active.collect(&:id)
        @route = {
            :group => {:name => "Региональные товары", :path => regions_url, :class => "region"},
            :item => {:name => region.name, :path => region_url(region), :id => region.id}
        }
      else
        not_found
    end
    @type = (TypeTag.find_by_id params[:id] or not_found)
    @products = @type.products_active.where(:id => product_ids).page params[:page]
  end
end
