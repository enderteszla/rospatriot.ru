module ApplicationHelper
  def product_group_type_url(group, id, type_id, product_id)
    case group
      when "subject"
        product_subject_type_url :subject_id => id, :type_id => type_id, :id => product_id
      when "category"
        product_category_type_url :category_id => id, :type_id => type_id, :id => product_id
      when "region"
        product_region_type_url :region_id => id, :type_id => type_id, :id => product_id
      else
    end
  end
end
