class ApplicationMailer < ActionMailer::Base
  default from: "info@rospatriot.ru"
  layout 'mailer'
end
