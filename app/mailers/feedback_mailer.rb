class FeedbackMailer < ApplicationMailer
  def notify(message)
    @message = message
    mail :to => "zakaz@rospatriot.ru", :subject => "Обратная связь"
  end
end
