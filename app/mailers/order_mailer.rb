class OrderMailer < ApplicationMailer
  def notify(records)
    @records = records.map(&:symbolize_keys)
    mail :to => "zakaz@rospatriot.ru", :subject => "Заявка"
  end

  def notify_user(records, email)
    @records = records.map(&:symbolize_keys)
    mail :to => email, :subject => "Заказ на сайте Роспатриот"
  end
end
