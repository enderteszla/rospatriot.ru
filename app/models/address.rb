class Address < ActiveRecord::Base
  belongs_to :user, :inverse_of => :addresses
  has_many :orders, :inverse_of => :address
  before_save :purge_defaults

  validates :first_name, :presence => true
  validates :phone, :presence => true
  validate :validate_yandex_address

  def validate_yandex_address
    if self.city.nil? || self.city.blank?
      self.errors.add(:city, " не может быть пустым")
    else
      self.errors.add(:city, " указан неверно") unless self.check_city
    end
    self.errors.add(:address, " указан неверно") unless self.check_address
  end

  def to_s
    [self._street, self._house, self._housing, self._build, self._flat, self.index, self.city]
        .select{|f| !f.blank?}.join(", ")
  end

  def full_name
    "#{self.last_name} #{self.first_name}"
  end

  def _street
    "ул. #{self.street}" unless self.street.try(:blank?)
  end

  def _house
    "дом #{self.house}" unless self.house.try(:blank?)
  end

  def _housing
    "корп. #{self.housing}" unless self.housing.try(:blank?)
  end

  def _build
    "стр. #{self.build}" unless self.build.try(:blank?)
  end

  def _flat
    "кв. #{self.flat}" unless self.flat.try(:blank?)
  end

  def purge_defaults
    return unless self.user
    self.user.addresses.where.not(:id => self.id).update_all :default => false if self.default
    self.default = true if self.user.addresses.where.not(:id => self.id).count == 0
  end

  protected
  def check_city
    (YandexDelivery.autocomplete({:term => self.city, :type => :locality}).try(:symbolize_keys)
         .try(:[], :suggestions).try(:count) or 0) > 0
  end

  def check_address
    (YandexDelivery.autocomplete({:term => [self.street, self.house, self._housing, self._build, self.city].select{|f|
      !f.blank?}.join(" "), :type => :address}).try(:symbolize_keys).try(:[], :suggestions).try(:count) or 0) > 0
  end
end
