class CategoryTag < ActiveRecord::Base
  has_and_belongs_to_many :products, inverse_of: :category_tags

  def products_active
    self.products.where :is_active => true
  end
end
