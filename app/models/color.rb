class Color < ActiveRecord::Base
  has_and_belongs_to_many :products, inverse_of: :colors

  def color
    "##{self.hex_code.gsub /^#/, ''}"
  end
end
