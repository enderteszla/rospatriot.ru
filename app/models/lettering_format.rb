class LetteringFormat < ActiveRecord::Base
  has_many :order_records, :inverse_of => :lettering_format
end
