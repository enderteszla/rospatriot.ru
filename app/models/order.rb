class Order < ActiveRecord::Base
  belongs_to :user, :inverse_of => :orders
  belongs_to :address, :inverse_of => :orders
  has_many :order_records, :inverse_of => :order

  enum :delivery_type => %w{courier pickup post}
  enum :payment_type => %w{cash credit}
  enum :status => %w{created confirmed in_progress completed}

  validates :delivery_type, :presence => true, :unless => :created?
  validates :payment_type, :presence => true, :unless => :created?

  def self.merge(lhand, rhand)
    if rhand
      lhand.order_records.update_all :order => rhand.order
      lhand.destroy
      rhand
    else
      lhand
    end
  end

  def total_price
    self.order_records.collect(&:total_price).reduce(:+) or 0
  end

  def total_quantity
    self.order_records.collect(&:quantity).reduce(:+) or 0
  end

  def empty?
    self.order_records.count == 0
  end

  def status_label
    case true
      when self.created?
        "Новый"
      when self.confirmed?
        "Подтверждён"
      when self.in_progress?
        "В обработке"
      when self.completed?
        "Выполнен"
      else
    end
  end

  def payment_status_label
    case true
      when self.paid?
        "Оплачен"
      else
        "Не оплачен"
    end
  end

  def delivery_label
    case true
      when self.courier?
        "Курьер"
      when self.pickup?
        "Самовывоз"
      when self.post?
        "Почта России"
      else
    end
  end

  def payment_label
    case true
      when self.cash?
        "наличными"
      when self.credit?
        "онлайн"
      else
    end
  end
end
