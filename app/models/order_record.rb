class OrderRecord < ActiveRecord::Base
  belongs_to :order, :inverse_of => :order_records
  belongs_to :product, :inverse_of => :order_records
  belongs_to :lettering_format, :inverse_of => :order_records

  def real_price
    (self.lettering ? 150 : 0) + (self.price or 0) + (self.lettering_format.try(:price) or 0)
  end

  def total_price
    self.real_price * (self.quantity or 0)
  end

  def final_lettering
    self.lettering ? "#{self.lettering_text} (#{self.lettering_color})" : "нет"
  end
end
