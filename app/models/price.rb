class Price < ActiveRecord::Base
  has_many :products, inverse_of: :price

  def real_price
    self.price_discount ? self.price_discount : self.price
  end
end
