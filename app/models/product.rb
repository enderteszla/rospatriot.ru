class Product < ActiveRecord::Base
  include PgSearch

  has_and_belongs_to_many :category_tags, inverse_of: :products
  has_and_belongs_to_many :type_tags, inverse_of: :products
  has_and_belongs_to_many :subject_tags, inverse_of: :products
  has_and_belongs_to_many :region_tags, inverse_of: :products
  has_and_belongs_to_many :colors, inverse_of: :products
  has_and_belongs_to_many :sizes, inverse_of: :products
  belongs_to :price, :inverse_of => :products
  belongs_to :product_dynamic_content, :inverse_of => :products
  has_many :order_records, :inverse_of => :product

  pg_search_scope :search,
                  :against => :name,
                  :associated_against => {
                      :category_tags => :name,
                      :subject_tags => :name,
                      :region_tags => :name
                  }

  mount_uploader :front_image, ProductFrontImageUploader
  mount_uploader :back_image, ProductBackImageUploader

  def self.active
    self.where :is_active => true
  end

  def _price
    self.price.try(:price) or 0
  end

  def _price_discount
    self.price.try :price_discount
  end

  def real_price
    self.price.try(:real_price) or 0
  end
end
