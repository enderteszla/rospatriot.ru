class ProductDynamicContent < ActiveRecord::Base
  has_many :products, :inverse_of => :product_dynamic_contents
end
