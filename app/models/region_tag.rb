class RegionTag < ActiveRecord::Base
  has_and_belongs_to_many :products, inverse_of: :region_tags

  mount_uploader :image, RegionImageUploader
  mount_uploader :plug, RegionPlugUploader

  def products_active
    self.products.where :is_active => true
  end
end
