class Size < ActiveRecord::Base
  has_and_belongs_to_many :products, inverse_of: :sizes

  def name
    "#{self.code} (#{self.label})"
  end
end
