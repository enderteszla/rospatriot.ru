class SubjectTag < ActiveRecord::Base
  has_and_belongs_to_many :products, inverse_of: :subject_tags

  mount_uploader :image, SubjectImageUploader

  def products_active
    self.products.where :is_active => true
  end
end
