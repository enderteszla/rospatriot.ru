class TypeTag < ActiveRecord::Base
  has_and_belongs_to_many :products, inverse_of: :type_tags

  def products_active
    self.products.where :is_active => true
  end
end
