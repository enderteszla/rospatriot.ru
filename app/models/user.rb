class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :omniauthable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable, :async

  has_many :addresses, :inverse_of => :user
  has_many :orders, :inverse_of => :user

  def full_name
    "#{self.last_name} #{self.first_name}"
  end

  def address
    self.addresses.find_by :default => true
  end
end
