Rails.application.routes.draw do
  get 'feedback_messages/new'

  get 'feedback_messages/create'

  root 'site#index'

  devise_for :users, :controllers => {
      :confirmations => "users/confirmations",
      :omniauth_callbacks => "users/omniauth_callbacks",
      :passwords => "users/passwords",
      :registrations => "users/registrations",
      :sessions => "users/sessions",
      :unlocks => "users/unlocks"
  }
  devise_for :admins, :controllers => {
      :sessions => "admins/sessions"
  }

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  get "/404", :to => "errors#not_found"
  get "/422", :to => "errors#unacceptable"
  get "/500", :to => "errors#internal_error"

  post "check", :to => "orders#check"
  post "pay", :to => "orders#pay"
  get "pay", :to => "orders#pay"
  get "success", :to => "orders#success"
  get "fail", :to => "orders#fail"

  get "search", :to => "products#search"
  post "search", :to => "products#search_live"
  get "shop", :to => "products#index"
  get "shop/product/:id", :to => "products#show", :by => "shop", :as => "product"
  get "shop/:type_id/product/:id", :to => "products#show", :by => "shop", :as => "product_type"

  resources :order_records, :only => [:destroy, :update]
  resources :orders, :only => [:index, :edit, :show, :update], :path => :cart
  resources :products, :only => [:create], :path => :cart

  get "subjects", :to => "subject_tags#index"
  get "subject/:id", :to => "subject_tags#show", :as => "subject"
  get "subject/:subject_id/product/:id", :to => "products#show", :by => "subject", :as => "product_subject"
  get "subject/:subject_id/:id", :to => "type_tags#show", :by => "subject", :as => "subject_type"
  get "subject/:subject_id/:type_id/product/:id", :to => "products#show", :by => "subject", :as => "product_subject_type"

  get "regions", :to => "region_tags#index"
  get "region/:id", :to => "region_tags#show", :as => "region"
  get "region/:id/video", :to => "region_tags#video", :as => "region_video"
  get "region/:region_id/product/:id", :to => "products#show", :by => "region", :as => "product_region"
  get "region/:region_id/:id", :to => "type_tags#show", :by => "region", :as => "region_type"
  get "region/:region_id/:type_id/product/:id", :to => "products#show", :by => "region", :as => "product_region_type"

  get "categories", :to => "category_tags#index"
  get "category/:id", :to => "category_tags#show", :as => "category"
  get "category/:category_id/product/:id", :to => "products#show", :by => "category", :as => "product_category"
  get "category/:category_id/:id", :to => "type_tags#show", :by => "category", :as => "category_type"
  get "category/:category_id/:type_id/product/:id", :to => "products#show", :by => "category", :as => "product_category_type"

  get "mass_media", :to => "site#mass_media"
  get "news", :to => "site#news"
  get "may_9th", :to => "site#may_9th"
  get "about", :to => "site#about"
  get "competition", :to => "site#competition"
  get "exclusive", :to => "site#exclusive"
  get "exclusive_gallery", :to => "site#exclusive_gallery"
  get "exclusive_popup", :to => "site#exclusive_popup"
  get "oferta_popup", :to => "site#oferta_popup"
  get "video_popup", :to => "site#video_popup"
  get "product_popup", :to => "site#product_popup"
  get "me", :to => "site#account"

  resources :feedback_messages, :only => [:new, :create], :path => :contacts

  resources :addresses, :only => [:index, :create, :update, :destroy]
end
