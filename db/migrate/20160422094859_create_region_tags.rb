class CreateRegionTags < ActiveRecord::Migration
  def change
    create_table :region_tags do |t|
      t.string :video
      t.string :name
      t.text :description
      t.string :image

      t.timestamps null: false
    end
  end
end
