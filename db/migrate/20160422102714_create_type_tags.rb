class CreateTypeTags < ActiveRecord::Migration
  def change
    create_table :type_tags do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
