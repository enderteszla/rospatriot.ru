class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :front_image
      t.string :back_image
      t.string :name
      t.integer :price
      t.integer :price_discount
      t.boolean :is_specialty
      t.string :sku
      t.string :material

      t.timestamps null: false
    end
  end
end
