class CreateJoinTableProductCategoryTag < ActiveRecord::Migration
  def change
    create_join_table :products, :category_tags do |t|
      # t.index [:product_id, :category_tag_id]
      # t.index [:category_tag_id, :product_id]
    end
  end
end
