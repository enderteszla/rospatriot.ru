class CreateJoinTableProductRegionTag < ActiveRecord::Migration
  def change
    create_join_table :products, :region_tags do |t|
      # t.index [:product_id, :region_tag_id]
      # t.index [:region_tag_id, :product_id]
    end
  end
end
