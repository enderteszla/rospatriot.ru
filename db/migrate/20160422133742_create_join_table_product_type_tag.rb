class CreateJoinTableProductTypeTag < ActiveRecord::Migration
  def change
    create_join_table :products, :type_tags do |t|
      # t.index [:product_id, :type_tag_id]
      # t.index [:type_tag_id, :product_id]
    end
  end
end
