class CreateJoinTableProductSubjectTag < ActiveRecord::Migration
  def change
    create_join_table :products, :subject_tags do |t|
      # t.index [:product_id, :subject_tag_id]
      # t.index [:subject_tag_id, :product_id]
    end
  end
end
