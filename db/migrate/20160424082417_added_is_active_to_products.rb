class AddedIsActiveToProducts < ActiveRecord::Migration
  def change
    add_column :products, :is_active, :boolean, :null => false, :default => true
  end
end
