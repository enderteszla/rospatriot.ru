class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :first_name
      t.string :last_name
      t.string :phone
      t.string :email
      t.references :region, index: true, foreign_key: true
      t.string :address
      t.integer :delivery_type_cd
      t.integer :payment_type_cd
      t.string :promo_code
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
