class CreateOrderRecords < ActiveRecord::Migration
  def change
    create_table :order_records do |t|
      t.references :order, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true
      t.integer :price
      t.integer :quantity
      t.string :color
      t.string :size

      t.timestamps null: false
    end
  end
end
