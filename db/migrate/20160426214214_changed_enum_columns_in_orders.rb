class ChangedEnumColumnsInOrders < ActiveRecord::Migration
  def change
    rename_column :orders, :delivery_type_cd, :delivery_type
    rename_column :orders, :payment_type_cd, :payment_type
    rename_column :orders, :status_cd, :status
  end
end
