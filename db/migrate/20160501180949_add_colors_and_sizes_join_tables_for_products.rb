class AddColorsAndSizesJoinTablesForProducts < ActiveRecord::Migration
  def change
    create_join_table :products, :colors
    create_join_table :products, :sizes
  end
end
