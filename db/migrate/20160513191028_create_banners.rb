class CreateBanners < ActiveRecord::Migration
  def change
    create_table :banners do |t|
      t.string :image
      t.text :content

      t.timestamps null: false
    end
  end
end
