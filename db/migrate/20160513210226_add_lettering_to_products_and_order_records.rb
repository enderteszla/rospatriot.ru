class AddLetteringToProductsAndOrderRecords < ActiveRecord::Migration
  def change
    add_column :products, :lettering, :boolean, :null => false, :default => false
    add_column :order_records, :lettering, :boolean, :null => false, :default => false
    add_column :order_records, :lettering_color, :string
    add_column :order_records, :lettering_text, :string
  end
end
