class AddPaidToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :paid, :boolean, :null => false, :default => false
  end
end
