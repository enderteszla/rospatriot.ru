class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.references :user, index: true, foreign_key: true
      t.string :city
      t.string :street
      t.string :house
      t.string :housing
      t.string :build
      t.string :flat
      t.string :index
      t.string :phone
      t.boolean :default, :null => false, :default => true

      t.timestamps null: false
    end
  end
end
