class DropAddressFieldsFromOrders < ActiveRecord::Migration
  def change
    remove_column :orders, :address, :string
    remove_column :orders, :first_name, :string
    remove_column :orders, :last_name, :string
    remove_column :orders, :phone, :string
    remove_column :orders, :email, :string
    remove_reference :orders, :region

    drop_table :regions do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
