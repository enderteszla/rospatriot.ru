class AddYandexDeliveryIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :yandex_delivery_id, :integer
  end
end
