class MoveProductsPricesToSeparateTable < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.integer :price
      t.integer :price_discount
    end
    add_reference :products, :price, :index => true
    rename_column :products, :price, :price_
    reversible do |dir|
      Product.all.each do |product|
        dir.up {
          (Price.find_or_create_by :price => product.price_, :price_discount => product.price_discount).products << product
        }
        dir.down {
          product.update :price_ => product.price.price, :price_discount => product.price.price_discount
        }
      end
    end
    remove_column :products, :price_, :integer
    remove_column :products, :price_discount, :integer
  end
end
