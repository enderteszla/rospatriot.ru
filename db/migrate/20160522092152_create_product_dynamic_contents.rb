class CreateProductDynamicContents < ActiveRecord::Migration
  def change
    create_table :product_dynamic_contents do |t|
      t.string :neckline
      t.string :length
      t.string :sleeve_type
      t.string :style
      t.string :constructive_items
      t.string :decorative_items
      t.string :sleeve
      t.string :season
      t.string :sex
      t.string :brand_country
      t.string :manufacturer_country
      t.string :equipment
      t.text :garment_care
      t.timestamps null: false
    end
    add_reference :products, :product_dynamic_content
  end
end
