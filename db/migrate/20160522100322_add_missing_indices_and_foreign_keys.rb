class AddMissingIndicesAndForeignKeys < ActiveRecord::Migration
  def change
    add_index :category_tags_products, :category_tag_id
    add_foreign_key :category_tags_products, :category_tags
    add_index :category_tags_products, :product_id
    add_foreign_key :category_tags_products, :products

    add_index :colors_products, :color_id
    add_foreign_key :colors_products, :colors
    add_index :colors_products, :product_id
    add_foreign_key :colors_products, :products

    add_foreign_key :products, :prices
    add_index :products, :product_dynamic_content_id
    add_foreign_key :products, :product_dynamic_contents

    add_index :products_region_tags, :region_tag_id
    add_foreign_key :products_region_tags, :region_tags
    add_index :products_region_tags, :product_id
    add_foreign_key :products_region_tags, :products

    add_index :products_sizes, :size_id
    add_foreign_key :products_sizes, :sizes
    add_index :products_sizes, :product_id
    add_foreign_key :products_sizes, :products

    add_index :products_subject_tags, :subject_tag_id
    add_foreign_key :products_subject_tags, :subject_tags
    add_index :products_subject_tags, :product_id
    add_foreign_key :products_subject_tags, :products

    add_index :products_type_tags, :type_tag_id
    add_foreign_key :products_type_tags, :type_tags
    add_index :products_type_tags, :product_id
    add_foreign_key :products_type_tags, :products
  end
end
