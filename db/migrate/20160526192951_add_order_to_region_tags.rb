class AddOrderToRegionTags < ActiveRecord::Migration
  def change
    add_column :region_tags, :order, :integer
  end
end
