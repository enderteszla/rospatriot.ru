class CreateLetteringFormats < ActiveRecord::Migration
  def change
    create_table :lettering_formats do |t|
      t.string :name
      t.integer :price

      t.timestamps null: false
    end
  end
end
