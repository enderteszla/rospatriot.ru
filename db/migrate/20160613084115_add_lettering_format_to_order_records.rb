class AddLetteringFormatToOrderRecords < ActiveRecord::Migration
  def change
    add_reference :order_records, :lettering_format, index: true, foreign_key: true
  end
end
