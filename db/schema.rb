# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160613084115) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "city"
    t.string   "street"
    t.string   "house"
    t.string   "housing"
    t.string   "build"
    t.string   "flat"
    t.string   "index"
    t.string   "phone"
    t.boolean  "default",    default: true, null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "first_name"
    t.string   "last_name"
  end

  add_index "addresses", ["user_id"], name: "index_addresses_on_user_id", using: :btree

  create_table "admins", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "articles", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "banners", force: :cascade do |t|
    t.string   "image"
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "category_tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "category_tags_products", id: false, force: :cascade do |t|
    t.integer "product_id",      null: false
    t.integer "category_tag_id", null: false
  end

  add_index "category_tags_products", ["category_tag_id"], name: "index_category_tags_products_on_category_tag_id", using: :btree
  add_index "category_tags_products", ["product_id"], name: "index_category_tags_products_on_product_id", using: :btree

  create_table "colors", force: :cascade do |t|
    t.string   "name"
    t.string   "label"
    t.string   "hex_code"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "colors_products", id: false, force: :cascade do |t|
    t.integer "product_id", null: false
    t.integer "color_id",   null: false
  end

  add_index "colors_products", ["color_id"], name: "index_colors_products_on_color_id", using: :btree
  add_index "colors_products", ["product_id"], name: "index_colors_products_on_product_id", using: :btree

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "feedback_messages", force: :cascade do |t|
    t.string   "name"
    t.string   "phone"
    t.string   "email"
    t.text     "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lettering_formats", force: :cascade do |t|
    t.string   "name"
    t.integer  "price"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "news", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.string   "image"
    t.datetime "date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "order_records", force: :cascade do |t|
    t.integer  "order_id"
    t.integer  "product_id"
    t.integer  "price"
    t.integer  "quantity"
    t.string   "color"
    t.string   "size"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.boolean  "lettering",           default: false, null: false
    t.string   "lettering_color"
    t.string   "lettering_text"
    t.integer  "lettering_format_id"
  end

  add_index "order_records", ["lettering_format_id"], name: "index_order_records_on_lettering_format_id", using: :btree
  add_index "order_records", ["order_id"], name: "index_order_records_on_order_id", using: :btree
  add_index "order_records", ["product_id"], name: "index_order_records_on_product_id", using: :btree

  create_table "orders", force: :cascade do |t|
    t.integer  "delivery_type"
    t.integer  "payment_type"
    t.string   "promo_code"
    t.integer  "user_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "status"
    t.boolean  "paid",               default: false, null: false
    t.integer  "address_id"
    t.integer  "yandex_delivery_id"
  end

  add_index "orders", ["address_id"], name: "index_orders_on_address_id", using: :btree
  add_index "orders", ["user_id"], name: "index_orders_on_user_id", using: :btree

  create_table "prices", force: :cascade do |t|
    t.integer "price"
    t.integer "price_discount"
  end

  create_table "product_dynamic_contents", force: :cascade do |t|
    t.string   "neckline"
    t.string   "length"
    t.string   "sleeve_type"
    t.string   "style"
    t.string   "constructive_items"
    t.string   "decorative_items"
    t.string   "sleeve"
    t.string   "season"
    t.string   "sex"
    t.string   "brand_country"
    t.string   "manufacturer_country"
    t.string   "equipment"
    t.text     "garment_care"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "front_image"
    t.string   "back_image"
    t.string   "name"
    t.boolean  "is_specialty"
    t.string   "sku"
    t.string   "material"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.boolean  "is_active",                  default: true,  null: false
    t.boolean  "lettering",                  default: false, null: false
    t.integer  "price_id"
    t.integer  "product_dynamic_content_id"
  end

  add_index "products", ["price_id"], name: "index_products_on_price_id", using: :btree
  add_index "products", ["product_dynamic_content_id"], name: "index_products_on_product_dynamic_content_id", using: :btree

  create_table "products_region_tags", id: false, force: :cascade do |t|
    t.integer "product_id",    null: false
    t.integer "region_tag_id", null: false
  end

  add_index "products_region_tags", ["product_id"], name: "index_products_region_tags_on_product_id", using: :btree
  add_index "products_region_tags", ["region_tag_id"], name: "index_products_region_tags_on_region_tag_id", using: :btree

  create_table "products_sizes", id: false, force: :cascade do |t|
    t.integer "product_id", null: false
    t.integer "size_id",    null: false
  end

  add_index "products_sizes", ["product_id"], name: "index_products_sizes_on_product_id", using: :btree
  add_index "products_sizes", ["size_id"], name: "index_products_sizes_on_size_id", using: :btree

  create_table "products_subject_tags", id: false, force: :cascade do |t|
    t.integer "product_id",     null: false
    t.integer "subject_tag_id", null: false
  end

  add_index "products_subject_tags", ["product_id"], name: "index_products_subject_tags_on_product_id", using: :btree
  add_index "products_subject_tags", ["subject_tag_id"], name: "index_products_subject_tags_on_subject_tag_id", using: :btree

  create_table "products_type_tags", id: false, force: :cascade do |t|
    t.integer "product_id",  null: false
    t.integer "type_tag_id", null: false
  end

  add_index "products_type_tags", ["product_id"], name: "index_products_type_tags_on_product_id", using: :btree
  add_index "products_type_tags", ["type_tag_id"], name: "index_products_type_tags_on_type_tag_id", using: :btree

  create_table "region_tags", force: :cascade do |t|
    t.string   "video"
    t.string   "name"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "plug"
    t.integer  "order"
  end

  create_table "sizes", force: :cascade do |t|
    t.string   "code"
    t.string   "label"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subject_tags", force: :cascade do |t|
    t.string   "name"
    t.string   "image"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "type_tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "first_name",             default: "", null: false
    t.string   "last_name",              default: "", null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  add_foreign_key "addresses", "users"
  add_foreign_key "category_tags_products", "category_tags"
  add_foreign_key "category_tags_products", "products"
  add_foreign_key "colors_products", "colors"
  add_foreign_key "colors_products", "products"
  add_foreign_key "order_records", "lettering_formats"
  add_foreign_key "order_records", "orders"
  add_foreign_key "order_records", "products"
  add_foreign_key "orders", "addresses"
  add_foreign_key "orders", "users"
  add_foreign_key "products", "prices"
  add_foreign_key "products", "product_dynamic_contents"
  add_foreign_key "products_region_tags", "products"
  add_foreign_key "products_region_tags", "region_tags"
  add_foreign_key "products_sizes", "products"
  add_foreign_key "products_sizes", "sizes"
  add_foreign_key "products_subject_tags", "products"
  add_foreign_key "products_subject_tags", "subject_tags"
  add_foreign_key "products_type_tags", "products"
  add_foreign_key "products_type_tags", "type_tags"
end
