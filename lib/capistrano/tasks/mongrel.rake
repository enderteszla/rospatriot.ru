namespace :mongrel do
  desc "Starts mongrel web server"
  task :start do
    on roles(:app) do
      execute "#{shared_path}/mongrel start #{fetch(:rails_env)}"
    end
  end

  desc "Stops mongrel web server"
  task :stop do
    on roles(:app) do
      execute "#{shared_path}/mongrel stop #{fetch(:rails_env)}"
    end
  end

  desc "Restarts mongrel web server"
  task :restart do
    on roles(:app) do
      execute "#{shared_path}/mongrel restart #{fetch(:rails_env)}"
    end
  end

  desc "Reloads mongrel web server"
  task :reload do
    on roles(:app) do
      execute "#{shared_path}/mongrel reload #{fetch(:rails_env)}"
    end
  end

  desc "Show help for mongrel web server"
  task :help do
    on roles(:app) do
      execute "#{shared_path}/mongrel help #{fetch(:rails_env)}"
    end
  end
end