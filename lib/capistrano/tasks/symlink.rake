namespace :symlink do
  task :finalize do
    on roles(:app) do
      execute "ln -nfs #{release_path} #{fetch(:app_path)}" if fetch(:app_path)
    end
  end
end