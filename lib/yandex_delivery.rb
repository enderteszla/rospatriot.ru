require 'net/http'

class YandexDelivery
  def self.uri
    Rails.configuration.x.yandex_delivery['uri']
  end

  def self.client_id
    Rails.configuration.x.yandex_delivery['client_id']
  end

  def self.sender_id
    Rails.configuration.x.yandex_delivery['sender_id']
  end

  def self.warehouse_id
    Rails.configuration.x.yandex_delivery['warehouse_id']
  end

  def self.requisite_id
    Rails.configuration.x.yandex_delivery['requisite_id']
  end

  def self.secret(method)
    Rails.configuration.x.yandex_delivery['secrets'][method]
  end

  def self.append_key(key, add)
    key ? "#{key}[#{add}]" : add
  end

  def self.pack(data, _key = nil)
    return data.keys.map{ |key| pack data[key], append_key(_key, key) } if data.kind_of? Hash
    return (0..data.length - 1).map{ |i| pack data[i], append_key(_key, i) } if data.kind_of? Array
    {_key => data}
  end

  def self.sign(method, data)
    Digest::MD5.hexdigest(data.keys.sort.map{ |key| data[key] }.join + secret(method))
  end

  def self.make_request(method, data = {})
    uri = URI "#{self.uri}/#{method}"
    proc_data = pack(data.merge({:client_id => client_id}).merge({:sender_id => sender_id}))
        .flatten.reduce({}, :merge).stringify_keys
    proc_data["secret_key"] = sign method, proc_data
    Rails.logger.debug "Yandex Delivery request: #{proc_data.to_json}"
    response = JSON.parse Net::HTTP.post_form(uri, proc_data).body
    Rails.logger.debug "Yandex Delivery response: #{response.to_json}"
    response['status'] == 'ok' ? response['data'] : nil
  end

  def self.get_sender_info
    make_request "getSenderInfo"
  end

  def self.create_order(order)
    items = order.order_records.map{ |record| {
        :orderitem_article => record.product.sku,
        :orderitem_name => record.product.name,
        :orderitem_quantity => record.quantity,
        :orderitem_cost => record.price
    }}
    address = order.address
    data = {
        :recipient_first_name => address.first_name,
        :recipient_last_name => address.last_name,
        :recipient_phone => address.phone,
        :recipient_email => order.user.email,
        :delivery_point_city => address.city,
        :delivery_point_street => address.street,
        :delivery_point_house => address.house,
        :delivery_point_housing => address.housing,
        :delivery_point_build => address.build,
        :delivery_point_flat => address.flat,
        :delivery_point_index => address.index,
        :order_items => items,
        :order_num => order.id
    }
    make_request("createOrder", data).try(:[], 'order').try(:[], 'id')
  end

  def self.get_order_info(id)
    make_request "getOrderInfo", {:order_id => id}
  end

  def self.autocomplete(_params)
    make_request "autocomplete", _params
  end
end
