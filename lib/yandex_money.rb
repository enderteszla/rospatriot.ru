module YandexMoney
  class Helper
    def self.secret
      Rails.configuration.x.yandex_money['secret']
    end

    def self.uri
      Rails.configuration.x.yandex_money['uri']
    end

    def self.shop_id
      Rails.configuration.x.yandex_money['shop_id']
    end

    def self.scid
      Rails.configuration.x.yandex_money['scid']
    end

    def self.host
      Rails.configuration.x.yandex_money['host']
    end

    def self.signature(_params)
      Digest::MD5.hexdigest("#{_params[:action]};#{_params[:orderSumAmount]};#{_params[:orderSumCurrencyPaycash]};#{_params[:orderSumBankPaycash]};#{_params[:shopId]};#{_params[:invoiceId]};#{_params[:customerNumber]};#{self.secret}").upcase
    end
  end

  class RequestGenerator
    require 'action_view'
    include ActionView::Helpers::NumberHelper

    def generate(order_id)
      order = Order.find_by_id order_id
      _params = {
          :shopId => YandexMoney::Helper.shop_id,
          :scid => YandexMoney::Helper.scid,
          :sum => order.total_price,
          :customerNumber => (order.user_id or 0),
          :orderNumber => order_id,
          :shopSuccessURL => Rails.application.routes.url_helpers.order_url(:id => order_id, :host => YandexMoney::Helper.host),
          :shopFailURL => Rails.application.routes.url_helpers.order_url(:id => order_id, :host => YandexMoney::Helper.host)
      }

      uri = URI YandexMoney::Helper.uri
      uri.query = _params.to_query
      uri.to_s
    end
  end

  class ResponseGenerator
    def check(_params)
      _params[:action] = "checkOrder"
      return respond _params, 1 unless _params[:md5] == YandexMoney::Helper.signature(_params)
      return respond _params, 200 unless (order = Order.find_by_id _params[:orderNumber])
      return respond _params, 100, "Неверная сумма заказа" unless _params[:orderSumAmount].to_i == order.total_price
      respond _params
    end

    def pay(_params)
      _params[:action] = "paymentAviso"
      return respond _params, 1 unless _params[:md5] == YandexMoney::Helper.signature(_params)
      return respond _params, 200 unless (order = Order.find_by_id _params[:orderNumber])
      order.update :paid => true, :yandex_delivery_id => (YandexDelivery.create_order order)
      respond _params
    end

    def respond(_params, code = 0, message = nil)
      date = DateTime.now.strftime "%Y-%m-%dT%H:%M:%S.%3N%:z"
      if code == 100
        "<#{_params[:action]}Response performedDatetime=\"#{date}\" " +
            "code=\"#{code}\" invoiceId=\"#{_params[:invoiceId]}\" shopId=\"#{_params[:shopId]}\" " +
            "message=\"#{message}\"/>"
      else
        "<#{_params[:action]}Response performedDatetime=\"#{date}\" " +
            "code=\"#{code}\" invoiceId=\"#{_params[:invoiceId]}\" shopId=\"#{_params[:shopId]}\"/>"
      end
    end
  end
end